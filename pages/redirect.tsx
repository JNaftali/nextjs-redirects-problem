import type { GetServerSideProps } from 'next';

export default function RedirectPage() {
  return <div>You should never see this</div>;
}

export const getServerSideProps: GetServerSideProps = async () => ({
  redirect: { permanent: true, destination: 'https://www.google.com' },
});
